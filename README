PACKAGE DOCUMENTATION

package revel_qbs
    import "bitbucket.org/ukaszg/revel_qbs"

    This plugin provides qms[1] orm support for the application. Also, it
    will auto-close/rollback transaction, if your controler HasQbs.

    Configuration options:

	db.driver - required, one of: postgres, mysql, oracle, sqlite3.
	db.spec - required, this one gets passed to the driver.
	db.name - required.
	db.log.sql - optional, log level for queries.
	db.log.error - optional, log level for errors.

    [1]: github.com/coocood/qbs


VARIABLES

var BeginTxnInterceptor revel.InterceptorFunc = func(c *revel.Controller) revel.Result {
    if h, ok := c.AppController.(HasQbs); ok {
        if err := h.GetQbs().Begin(); err != nil {
            return c.RenderError(err)
        }
    }
    return nil
}
    Begins a transaction.


FUNCTIONS

func GetQbs(c *revel.Controller) *qbs.Qbs
    Extracts HasQbs from Controller and runs HasQbs.GetQbs()

func RegisterDb()


TYPES

type HasQbs struct {
    // contains filtered or unexported fields
}
    Adds qbs support.


func (h *HasQbs) GetQbs() *qbs.Qbs
    Get cached Qbs instance, initialize if needed.



