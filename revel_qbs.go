// This plugin provides qms[1] orm support for the application.
// Also, it will auto-close/rollback transaction, if your controler HasQbs.
//
// Configuration options:
//   db.driver - required, one of: postgres, mysql, oracle, sqlite3.
//   db.spec - required, this one gets passed to the driver.
//   db.name - required.
//   db.log.sql - optional, log level for queries.
//   db.log.error - optional, log level for errors.
//
// [1]: github.com/coocood/qbs
package revel_qbs

import (
	"database/sql"
	"fmt"
	"github.com/coocood/qbs"
	"github.com/robfig/revel"
	"log"
)

var onMigration func(m *qbs.Migration) error

func RegisterDb() {

	configRequired := func(key string) string {
		value, found := revel.Config.String(key)
		if !found {
			revel.ERROR.Fatal(fmt.Sprintf("No configuration for %s found.", key))
		}
		return value
	}

	dialect := func(driver string) qbs.Dialect {
		defer (func() {
			if err := recover(); err != nil {
				msg := "DB driver %s not imported. For a list see: http://golang.org/s/sqldrivers"
				revel.ERROR.Fatal(fmt.Sprintf(msg, driver))
			}
		})()
		switch driver {
		case "postgres":
			return qbs.NewPostgres()
		case "mysql":
			return qbs.NewMysql()
		case "oracle":
			return qbs.NewOracle()
		case "sqlite3":
			return qbs.NewSqlite3()
		default:
			msg := "Unsupported driver `%s`, qbs has support for: %"
			revel.ERROR.Fatal(fmt.Sprintf(msg, driver, "postgres, mysql, oracle, sqlite3"))
			return nil
		}
	}

	logger := func(config string) *log.Logger {
		level := revel.Config.StringDefault(config, "")
		switch level {
		case "trace":
			return revel.TRACE
		case "info":
			return revel.INFO
		case "warn":
			return revel.WARN
		case "error":
			return revel.ERROR
		case "":
			return nil
		default:
			revel.ERROR.Println(fmt.Sprint("No such log level: %s", level))
			return nil
		}
	}

	// Read configuration.
	driver := configRequired("db.driver")
	spec := configRequired("db.spec")
	db_name := configRequired("db.name")

	// Open a connection.
	qbs.Register(driver, spec, db_name, dialect(driver))
	revel.INFO.Println("registered database "+db_name)

	// Register loggers.
	query := logger("db.log.sql")
	query.Println("test")
	errors := logger("db.log.error")
	errors.Println("test")
	qbs.SetLogger(query, errors)
}

// Begins a transaction.
var BeginTxnInterceptor revel.InterceptorFunc = func(c *revel.Controller) revel.Result {
	if h, ok := c.AppController.(HasQbs); ok {
		if err := h.GetQbs().Begin(); err != nil {
			return c.RenderError(err)
		}
	}
	return nil
}

// Adds qbs support.
type HasQbs struct {
	db *qbs.Qbs
}

// Get cached Qbs instance, initialize if needed.
func (h *HasQbs) GetQbs() *qbs.Qbs {
	if h.db == nil {
		if q, err := qbs.GetQbs(); err != nil {
			panic(err)
		} else {
			h.db = q
		}
	}
	return h.db
}

// Extracts HasQbs from Controller and runs HasQbs.GetQbs()
func GetQbs(c *revel.Controller) *qbs.Qbs {
	ac, ok := c.AppController.(HasQbs)
	if !ok {
		panic("Expected an instance of `revel_qbs.HasQbs`.")
	}
	return ac.GetQbs()
}

func noTxDone(err error) error {
	if err != sql.ErrTxDone {
		return err
	}
	return nil
}

// Commit transaction, if any.
func commit_transaction(c *revel.Controller) revel.Result {
	if ac, ok := c.AppController.(HasQbs); ok {
		if ac.db != nil {
			defer (func() { ac.db.Close(); ac.db = nil })()
			if ac.db.InTransaction() {
				if err := noTxDone(ac.db.Commit()); err != nil {
					return c.RenderError(err)
				}
			}
		}
	}
	return nil
}

// Rollback the active transaction, if any.
func rollback_transaction(c *revel.Controller) revel.Result {
	if ac, ok := c.AppController.(HasQbs); ok {
		if ac.db != nil {
			defer ac.db.Close()
			if ac.db.InTransaction() {
				if err := noTxDone(ac.db.Rollback()); err != nil {
					return c.RenderError(err)
				}
			}
		}
	}
	return nil
}

func init() {
	revel.InterceptMethod(commit_transaction, revel.AFTER)
	revel.InterceptMethod(rollback_transaction, revel.FINALLY)
}
